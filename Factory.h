#pragma once
#include <map>
#include <memory>


template<typename BaseType>
struct Creator {
	virtual std::shared_ptr<BaseType> create() = 0;
};


template<typename BaseType, typename ConcreteType>
struct ConcreteCreator : Creator<BaseType> {
	virtual std::shared_ptr<BaseType> create() {
		return std::make_shared<ConcreteType>();
	};
};


template<typename BaseType>
struct Factory
{
	static Factory& instance() {
		static Factory instance;
		return instance;
	}

	template<typename ConcreteType>
	bool registerType(int id) {
		assert(generators.find(id) == generators.end());
		generators.insert(std::pair{id, new ConcreteCreator<BaseType, ConcreteType>{}});
		return true;
	}

	template<typename ConcreteType>
	bool registerTypeWithNextId() {
		return registerType<ConcreteType>(static_cast<int>(generators.size()));
	}

	template<typename Creator>
	bool registerCreatorWithNextId(Creator* creator) {
		generators.insert(std::pair{static_cast<int>(generators.size()), creator});
		return true;
	}

	std::shared_ptr<BaseType> create(int id) {
		assert(generators.find(id) != generators.end());
		auto * creator = generators[id];
		assert(creator);
		return creator->create();
	}

private:
	Factory() = default;
	std::map<int, Creator<BaseType>*> generators;
};

