#pragma once
#include <cstdlib>
#include <stdexcept>
#include <array>
#include <map>
#include <Rectangle.h>


enum class Direction {
	UP,
	LEFT,
	RIGHT,
	DOWN
};


inline auto rightDirection(const Direction dir) {
	static constexpr auto clockwiseDirs = std::array<Direction, 4>{Direction::UP, Direction::RIGHT, Direction::DOWN, Direction::LEFT};
	for (auto i = 0u; i < clockwiseDirs.size(); ++i) {
		if (clockwiseDirs[i] == dir)
			return clockwiseDirs[(i + 1) % clockwiseDirs.size()];
	}
	throw std::logic_error("it can't newer be!");
}


inline auto directionToVector(const Direction dir) {
	switch (dir) {
	case Direction::RIGHT: return Point2i{1, 0};
	case Direction::DOWN: return Point2i{0, -1};
	case Direction::LEFT: return Point2i{-1, 0};
	case Direction::UP: return Point2i{0, 1};
	}
	throw std::logic_error("can't never be!");
}


inline bool isReverse(Direction a, Direction b){
	if (a == b)
		return false;
	auto va = directionToVector(a);
	auto vb = directionToVector(b);
	return (va.x + vb.x == 0) || (va.y + vb.y == 0);
}
