#pragma once


template<typename T>
struct Point2 {
	T x{};
	T y{};
};
using Point2i = Point2<int>;


template<typename T>
struct Size2 {
	T width{};
	T height{};
};
using Size2i = Size2<int>;


template<typename T>
struct Rectangle {
	constexpr Rectangle() = default;
	constexpr Rectangle(T x, T y, T w, T h) : pos{x, y}, size{w, h} {}
	Point2<T> pos{};
	Size2<T> size{};

	bool intersects(const Rectangle& other) const {
		return !((other.pos.x + other.size.width <= pos.x) ||
			(other.pos.y + other.size.height <= pos.y) ||
			(other.pos.x >= pos.x + size.width) ||
			(other.pos.y >= pos.y + size.height));
	}
};
using RectangleI = Rectangle<int>;
