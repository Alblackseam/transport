import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    visible: true
    width: 1280
    height: 1080
    title: qsTr("Scroll")

    ScrollView {
        anchors.fill: parent

        Row{
            Rectangle{
                id: scene
                width:  1024
                height:  768
                color: "black"
                clip: true


                Repeater {
                    width: parent.width
                    model: carsModel
                    delegate: Item{
                        Rectangle {
                            x: areaX
                            y: scene.height - areaY
                            width: areaW
                            height: areaH
                            color: "transparent"
                            border.color: "#77FF0000"
                            border.width: 1
                        }
                        Rectangle{
                            color: "red"
                            x: visX + 3
                            y: scene.height - (visY - 3)
                            width: visW - 6
                            height: visH - 6

                            Rectangle {
                                anchors.centerIn: parent
                                width: 30
                                height: 30
                                color: "transparent"
                                border.color: "green"

                                Rectangle {
                                    anchors.bottom: parent.bottom
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    width: 30
                                    height: 30.0 * fuel / carsModel.getMaxFuel()
                                    color: "green"
                                }
                            }
                            Text {
                                text: carType // + id
                            }
                        }
                    }
                } // Repeater
            }
            Column {
                Button{
                    width: trafficIntensity.width
                    checkable: true
                    text: "pause"
                    onCheckedChanged: carsModel.pause(checked)
                }

                Rectangle {
                    width: 1
                    height: 30
                    color: "transparent"
                }

                SpinBox {
                    id: trafficIntensity
                    from: 0
                    to: 100
                    value: carsModel.trafficIntensity
                    onValueChanged: carsModel.trafficIntensity = value;
                }
                Label {
                    text: "trafficIntensity"
                    horizontalAlignment: "AlignHCenter"
                    width: trafficIntensity.width
                }
            }
        }
    }
}
