#include "Car.h"
#include "Factory.h"


Car::Car(std::shared_ptr<CarEquipment> carEquipment, Direction direction, RectangleI rectangle)
	: dir {direction}
	, rect {rectangle}
	, equipment {carEquipment}
{
}

RectangleI Car::visualRect() const
{
	switch (dir) {
		case Direction::UP: return Rectangle{rect.pos.x + carHalfSize, rect.pos.y, carHalfSize, carSize};
		case Direction::DOWN: return Rectangle{rect.pos.x, rect.pos.y, carHalfSize, carSize};
		case Direction::RIGHT: return Rectangle{rect.pos.x, rect.pos.y - carHalfSize, carSize, carHalfSize};
		case Direction::LEFT: return Rectangle{rect.pos.x, rect.pos.y, carSize, carHalfSize};
	}
	throw std::logic_error("can't never be!");
}


void Car::move()
{
	auto [dx, dy] = directionToVector(dir);
	rect.pos.x += dx * speed;
	rect.pos.y += dy * speed;

	equipment->spend();
}

RectangleI Car::getFuturePos() const
{
	auto vec = directionToVector(dir);
	return RectangleI(rect.pos.x + vec.x * speed, rect.pos.y + vec.y * speed, rect.size.width, rect.size.height);
}

bool Car::isOtherCarAtRight(std::shared_ptr<Car> otherCar) const
{
	return dir == rightDirection(otherCar->dir);
}

int Car::getFuel() const
{
	return equipment->getFuel();
}

void Car::refill(int count)
{
	return equipment->refill(count);
}

std::shared_ptr<const CarEquipment> Car::getEquipment() const
{
	return equipment;
}



static auto GasEngineRegistered = Factory<CarEquipment>::instance().registerTypeWithNextId<GasEquipment>();
static auto ElectroRegistered = Factory<CarEquipment>::instance().registerTypeWithNextId<ElectricalEquipment>();
static auto HybridRegistered = Factory<CarEquipment>::instance().registerTypeWithNextId<HybridEquipment>();

