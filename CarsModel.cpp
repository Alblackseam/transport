#include <vector>
#include <array>
#include <set>
#include <map>
#include <algorithm>
#include "CarsModel.h"
#include "Factory.h"


CarsModel::CarsModel() {
	for (auto i = 0; i < initialCarsCount; ++i) {
		trySpawnCar();
	}
}

void CarsModel::mainLoop() {
	auto carsToDelay = nominateCarsToDelay();

	std::set<std::shared_ptr<Car>> refillingCars;
	std::copy_if(allCars.begin(), allCars.end(), std::insert_iterator(refillingCars, refillingCars.begin()), [](auto car) {
		if (car)
			return car->getFuel() == 0;
		else
			return false; });

	for(auto refillingCar: refillingCars) {
		carsToDelay.insert(refillingCar);
		refillingCar->refill(CarEquipment::MaxCarFuel);
	}

	for (auto additionalCarsToDelay = nominateCarsToDelayConsiderStopped(carsToDelay); !additionalCarsToDelay.empty(); additionalCarsToDelay = nominateCarsToDelayConsiderStopped(carsToDelay)) {
		carsToDelay.insert(additionalCarsToDelay.begin(), additionalCarsToDelay.end());
	}


	Rectangle visibleAreaRect{0, 0, AreaWidth, AreaHeight};

	for (auto carIt = std::begin(allCars); carIt != std::end(allCars);) {
		auto car = *carIt;
		auto found = carsToDelay.find(car) != std::end(carsToDelay);
		if (!found) {
			car->move();
		}

		if (car->rect.intersects(visibleAreaRect)) {
			++carIt;
		} else {
			carIt = allCars.erase(carIt);
		}
	}

	if (rand() % 1000 < trafficIntensity){
		trySpawnCar();
	}
}

auto CarsModel::getRandomCarParameters()
{
	using namespace std;

	static constexpr auto directionCount = 4;
	using Parameters = array<pair<Direction, RectangleI>, directionCount>;
	static constexpr Parameters carPropertiesByDirection{
		pair<Direction, RectangleI>{Direction::DOWN,  Rectangle(AreaWidth / 2,     AreaHeight, Car::carSize, Car::carSize)},
		pair<Direction, RectangleI>{Direction::UP,    Rectangle(AreaWidth / 2,   Car::carSize, Car::carSize, Car::carSize)},
		pair<Direction, RectangleI>{Direction::LEFT,  Rectangle(    AreaWidth, AreaHeight / 2, Car::carSize, Car::carSize)},
		pair<Direction, RectangleI>{Direction::RIGHT, Rectangle(            0, AreaHeight / 2, Car::carSize, Car::carSize)}
	};
	auto directionIndex = static_cast<Parameters::size_type>(rand() % directionCount);

	auto carType = rand() % 3;
	auto carDir = carPropertiesByDirection[directionIndex].first;
	auto carRect = carPropertiesByDirection[directionIndex].second;
	return std::tuple{carType, carDir, carRect};
}

void CarsModel::trySpawnCar()
{
	auto [carType, carDir, carRect] = getRandomCarParameters();
	if (!isCarIntersectsByOther(carRect)) {
		auto carEquipment = Factory<CarEquipment>::instance().create(carType);
		auto newCar = std::make_shared<Car>(carEquipment, carDir, carRect);
		allCars.push_back(newCar);
	}
}

std::set<std::shared_ptr<Car>> CarsModel::closedChainWithItem(std::shared_ptr<Car> item, const std::map<std::shared_ptr<Car>, std::shared_ptr<Car>>& cars)
{
	std::set<std::shared_ptr<Car>> closedChain;
	auto it = cars.find(item);

	while(it != std::end(cars)) {
		if (closedChain.find(it->second) != closedChain.end()) {
			return {};
		}

		closedChain.insert(it->second);

		if (it->second == item) {
			return closedChain;
		}

		it = cars.find(it->second);
	}
	return {};
}


std::set<std::shared_ptr<Car>> CarsModel::nominateCarsToDelay() const
{
	using namespace std;
	set<std::shared_ptr<Car>> result;

	map<std::shared_ptr<Car>, std::shared_ptr<Car>> carBlockedByCar;
	for (auto carIt = std::begin(allCars); carIt != std::end(allCars); ++carIt) {
		for (auto otherCarIt = carIt + 1; otherCarIt != std::end(allCars); ++otherCarIt) {
			auto car = *carIt;
			auto otherCar = *otherCarIt;
			if (car == otherCar)
				continue;

			if (isReverse(car->dir, otherCar->dir))
				continue;

			if (car->getFuturePos().intersects(otherCar->getFuturePos())) {

				if ((!car->rect.intersects(otherCar->getFuturePos())) && (!otherCar->rect.intersects(car->getFuturePos()))) {
					if (car->isOtherCarAtRight(otherCar)) {
						carBlockedByCar[car] = otherCar;
					} else {
						carBlockedByCar[otherCar] = car;
					}
				} else {
					if (!car->rect.intersects(otherCar->getFuturePos())) {
						carBlockedByCar[car] = otherCar;
					} else {
						carBlockedByCar[otherCar] = car;
					}
				}
			}
		}
	}

	while (!carBlockedByCar.empty()) {
		const auto& [blockedCar, blockReasonCar] = *std::begin(carBlockedByCar);

		auto closedChain = closedChainWithItem(blockedCar, carBlockedByCar);
		if (closedChain.empty()) {
			result.insert(blockedCar);
			carBlockedByCar.erase(blockedCar);
		} else {

			// find car with minX
			auto minXCar = *std::begin(closedChain);
			auto minX = minXCar->rect.pos.x;
			for (const auto& c : closedChain) {
				if (c->rect.pos.x < minX) {
					minXCar = c;
					minX = minXCar->rect.pos.x;
				}
			}

			if (!canCarMove(minXCar))
				minXCar = nullptr;

			std::for_each(std::begin(closedChain), std::end(closedChain), [minXCar, &carBlockedByCar, &result](std::shared_ptr<Car> car){
				if (car != minXCar)
					result.insert(car);
				carBlockedByCar.erase(car);
			});
		}
	}

	return result;
}

RectangleI CarsModel::getFuturePos(std::shared_ptr<Car> car, std::set<std::shared_ptr<Car>> stoppingCars) const
{
	if (stoppingCars.find(car) != std::end(stoppingCars))
		return car->rect;
	else
		return car->getFuturePos();
}


std::set<std::shared_ptr<Car>> CarsModel::nominateCarsToDelayConsiderStopped(std::set<std::shared_ptr<Car>>& stoppingCars) const
{
	using namespace std;
	set<std::shared_ptr<Car>> result;

	map<std::shared_ptr<Car>, std::shared_ptr<Car>> carBlockedByCar;
	for (auto carIt = std::begin(allCars); carIt != std::end(allCars); ++carIt) {
		for (auto otherCarIt = std::begin(allCars); otherCarIt != std::end(allCars); ++otherCarIt) {
			auto car = *carIt;
			auto otherCar = *otherCarIt;
			if (car == otherCar)
				continue;

			if (isReverse(car->dir, otherCar->dir))
				continue;

			if (getFuturePos(car, stoppingCars).intersects(getFuturePos(otherCar, stoppingCars))) {
				result.insert(car);
			}
		}
	}
	return result;
}


bool CarsModel::canCarMove(std::shared_ptr<Car> car) const
{
	for (auto otherCarIt = std::begin(allCars); otherCarIt != std::end(allCars); ++otherCarIt)
	{
		auto otherCar = *otherCarIt;
		if (car != otherCar)
			if (car->getFuturePos().intersects(otherCar->rect))
				return false;
	}
	return true;
}

bool CarsModel::isCarIntersectsByOther(const RectangleI& carRect) const
{
	for (auto otherCarIt = std::begin(allCars); otherCarIt != std::end(allCars); ++otherCarIt)
	{
		auto otherCar = *otherCarIt;
		if (carRect.intersects(otherCar->rect))
			return true;
	}
	return false;
}

