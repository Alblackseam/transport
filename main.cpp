#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <CarsModel.h>
#include <CarsModelWrapper.h>
#include <QQmlContext>
#include <iostream>


using namespace std;

int main(int argc, char *argv[]) {
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);

	assert(isReverse(Direction::UP, Direction::DOWN) == true);
	assert(isReverse(Direction::LEFT, Direction::RIGHT) == true);

	assert(isReverse(Direction::LEFT, Direction::LEFT) == false);
	assert(isReverse(Direction::UP, Direction::LEFT) == false);
	assert(isReverse(Direction::UP, Direction::RIGHT) == false);
	assert(isReverse(Direction::LEFT, Direction::UP) == false);
	assert(isReverse(Direction::LEFT, Direction::DOWN) == false);

	Rectangle visibleAreaRect{0, 0, 2000, 1000};
	assert(visibleAreaRect.intersects(Rectangle{50, -20, 50, 50}) == true);
	assert(visibleAreaRect.intersects(Rectangle{50, -60, 50, 50}) == false);
	assert(visibleAreaRect.intersects(Rectangle{-30, 20, 50, 50}) == true);
	assert(visibleAreaRect.intersects(Rectangle{-50, 20, 50, 50}) == false);
	assert(visibleAreaRect.intersects(Rectangle{-60, 20, 50, 50}) == false);



	CarsModelWrapper carsModelWrapper{nullptr};
	carsModelWrapper.carsModel = std::make_shared<CarsModel>();

	QQmlApplicationEngine engine;
	engine.rootContext()->setContextProperty("carsModel", &carsModelWrapper);

	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreated, &app, [url](QObject *obj, const QUrl &objUrl) {
		if (!obj && url == objUrl)
			QCoreApplication::exit(-1);
	}, Qt::QueuedConnection);
	engine.load(url);

	return app.exec();
}
