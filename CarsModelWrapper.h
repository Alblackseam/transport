#pragma once
#include <memory>
#include <QAbstractListModel>
#include <QTimer>
#include <QColor>
#include <QObject>
#include <QMetaEnum>
#include <QDebug>

#include "Rectangle.h"
#include "CarsModel.h"


class CarsModelWrapper : public QAbstractListModel {
	Q_OBJECT
	Q_PROPERTY(int trafficIntensity READ getTrafficIntensity WRITE setTrafficIntensity NOTIFY trafficIntensityChanged)
public:
	enum Roles { visX = Qt::UserRole + 1, visY, visW, visH, areaX, areaY, areaW, areaH, fuel, direction, colour, carType }; // id
	Q_ENUM(Roles);
	static constexpr auto timerIntervalMs = 100;

	std::shared_ptr<CarsModel> carsModel;

	explicit CarsModelWrapper(QObject *parent = nullptr)
		: QAbstractListModel(parent)
		, timer(nullptr)
	{
		QObject::connect(&timer, &QTimer::timeout, this, &CarsModelWrapper::tick);
		timer.setSingleShot(false);
		timer.start(timerIntervalMs);
	}

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override {
		const auto& value = carsModel->allCars[index.row()];
		switch (static_cast<Roles>(role)) {
		case Roles::visX: return value->visualRect().pos.x;
		case Roles::visY: return value->visualRect().pos.y;
		case Roles::visW: return value->visualRect().size.width;
		case Roles::visH: return value->visualRect().size.height;

		case Roles::areaX: return value->rect.pos.x;
		case Roles::areaY: return value->rect.pos.y;
		case Roles::areaW: return value->rect.size.width;
		case Roles::areaH: return value->rect.size.height;

		case Roles::fuel: return value->getFuel();
		case Roles::direction: return {};
		case Roles::colour: return QColor("green"); // value->type;
		//case Roles::id: return value->id;
		case Roles::carType:
			if (auto eq = value->getEquipment())
			{
				return QString(eq->equipmentTypeString().data());
			}
			return {};
		}
		return {};
	}


	int rowCount(const QModelIndex &parent = QModelIndex()) const override {
		Q_UNUSED(parent;)
		return carsModel->allCars.size();
	}

	virtual QHash<int, QByteArray> roleNames() const override {
		QHash<int, QByteArray> result;

		auto metaRoles = QMetaEnum::fromType<Roles>();
		for (int i = 0; i < metaRoles.keyCount(); ++i)
			result.insert(static_cast<int>(metaRoles.value(i)), metaRoles.key(i)); // qDebug() << metaRoles.key(i) << metaRoles.value(i);
		return result;
	}

private:
	QTimer timer;

Q_SIGNALS:
	void trafficIntensityChanged();

public slots:
	void tick() {
		if (carsModel){
			beginResetModel();
			for (int i = 0; i < 10; ++i)
				carsModel->mainLoop();
			endResetModel();
		}
	}

	void pause(bool pause) {
		if (pause) timer.stop();
		else timer.start(timerIntervalMs);
	}

	int getMaxFuel() {
		return CarEquipment::MaxCarFuel;
	}

	int getTrafficIntensity() {
		if (!carsModel)
			return 0;
		return carsModel->trafficIntensity;
	}

	void setTrafficIntensity(int value) {
		if (carsModel)
			carsModel->trafficIntensity = value;
	}
};
