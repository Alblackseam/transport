#pragma once
#include <vector>
#include <set>
#include <map>
#include <Car.h>


struct CarsModel {
	static constexpr int initialCarsCount = 10;
	static constexpr int  AreaWidth = 1024;
	static constexpr int  AreaHeight = 768;

	int trafficIntensity = 19;
	std::vector<std::shared_ptr<Car>> allCars;

	CarsModel();
	void mainLoop();

private:
	static auto getRandomCarParameters();
	static std::set<std::shared_ptr<Car>> closedChainWithItem(std::shared_ptr<Car> item, const std::map<std::shared_ptr<Car>, std::shared_ptr<Car>>& cars);

	void trySpawnCar();
	std::set<std::shared_ptr<Car>> nominateCarsToDelay() const;
	RectangleI getFuturePos(std::shared_ptr<Car> car, std::set<std::shared_ptr<Car>> stoppingCars) const;
	std::set<std::shared_ptr<Car>> nominateCarsToDelayConsiderStopped(std::set<std::shared_ptr<Car>>& stoppingCars) const;
	bool canCarMove(std::shared_ptr<Car> car) const;
	bool isCarIntersectsByOther(const RectangleI& carRect) const;
};

