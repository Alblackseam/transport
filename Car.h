#pragma once
#include <cstdlib>
#include <stdexcept>
#include <array>
#include <memory>
#include <map>
#include <string_view>
#include <cassert>
#include "Rectangle.h"
#include "Direction.h"




struct CarEquipment {
	static constexpr auto MaxCarFuel = 60;

	virtual ~CarEquipment() = default;
	virtual int getFuel() const = 0;
	virtual void refill(int count) = 0;
	virtual void spend() = 0;
	virtual std::string_view equipmentTypeString() const = 0;
};


struct ElectricalEquipment : CarEquipment {
	int getFuel() const override {
		return charge;
	}

	void refill(int count) override {
		charge += count;
	}

	void spend() override {
		charge--;
	}

	std::string_view equipmentTypeString() const override {
		return "Ele";
	}

protected:
	int charge {CarEquipment::MaxCarFuel};
};


struct GasEquipment : CarEquipment {
	int getFuel() const override {
		return gas;
	}

	void refill(int count) override {
		gas += count;
	}

	void spend() override {
		gas--;
	}

	std::string_view equipmentTypeString() const override {
		return "Gas";
	}

protected:
	int gas {CarEquipment::MaxCarFuel};
};


struct HybridEquipment : CarEquipment {
	void refill(int count) override {
		charge += count / 2;
		gas += count / 2 + count % 2;
	}

	int getFuel() const override {
		return charge + gas;
	}

	void spend() override {
		assert(charge >= 0);
		assert(gas >= 0);

		bool useElectro = rand() % 2 == 0;
		if (charge <= 0)
			useElectro = false;

		if (gas <= 0)
			useElectro = true;

		if (useElectro)
			charge--;
		else
			gas--;
	}

	std::string_view equipmentTypeString() const override {
		return "Hib";
	}


protected:
	int charge {CarEquipment::MaxCarFuel};
	int gas {CarEquipment::MaxCarFuel};
};



struct Car {
	static constexpr auto carSize = 100;
	static constexpr auto carHalfSize = carSize / 2;

	Car(std::shared_ptr<CarEquipment> carEquipment, Direction direction, RectangleI rectangle);

	RectangleI visualRect() const;
	void move();
	RectangleI getFuturePos() const;
	bool isOtherCarAtRight(std::shared_ptr<Car> otherCar) const;
	int getFuel() const;
	void refill(int count);
	std::shared_ptr<const CarEquipment> getEquipment() const;

	Direction dir;
	RectangleI rect{}; // area
	int speed {1};

private:
	std::shared_ptr<CarEquipment> equipment;
};

